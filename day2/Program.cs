﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day2
{
    class Program
    {
        static Dictionary<int, int> Pows = new Dictionary<int, int>();
        static void FindPowOfNumber(int value,params int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 1)
                    Pows.Add(0, array[i]);
                for (int j = 1; j <= 10; j++)
                    if (array[i] == Pow(value, j))
                        Pows.Add(j, array[i]);
            }
        }
        static int Pow(int number, int power)
        {
            if (power == 1)
                return number;
            return number * Pow(number, --power);
        }
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ').Select(x => int.Parse(x)).Distinct().ToArray();
            var value = int.Parse(Console.ReadLine());
            FindPowOfNumber(value, input);
            foreach(var finder in Pows)
                Console.WriteLine(value + "^" + finder.Key + " = " + finder.Value);
            Console.ReadKey();
        }
    }
}
