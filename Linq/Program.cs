﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int> {1,2,3,4,5,6 };
            var res = list.Where(x=>x<2);
            Console.WriteLine(res);
        }
    }
}
