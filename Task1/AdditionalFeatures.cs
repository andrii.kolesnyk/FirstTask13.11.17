﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    static class AdditionalFeatures
    {
        private static bool ValidatorForDouble(object obj)
        {
            if (obj is double)
            {
                Console.WriteLine("Your array is already exists. Press any button to continue...");
                Console.ReadKey();
                return true;
            }
            return false;
        }
        public static void CreateNewArray(ArrayList array)
        {
            if (ValidatorForDouble(array[0]))
                return;

            double[] resultArray = new double[array.Count];
            int[] intArrayForCalculation = new int[array.Count];
            array.CopyTo(intArrayForCalculation);
            intArrayForCalculation = intArrayForCalculation.OrderBy(x => x).ToArray<int>();
            if (intArrayForCalculation[0] != 0)
            {
                for (int i = 0; i < array.Count; i++)
                    resultArray[i] = (intArrayForCalculation[i] + intArrayForCalculation[array.Count-1]) / (double)intArrayForCalculation[0];
            }
            else
            {
                Console.WriteLine("You can't create new array because your min element is 0. Press any button to continue...");
                Console.ReadKey();
                return;
            }
            array.Clear();
            array.AddRange(resultArray);
            Console.WriteLine("New array is created. Press any button to continue...");
            Console.ReadKey();
        }
        public static string NumberToText(int n)
        {
            if (n < 0)
                return "Minus " + NumberToText(-n);
            else if (n == 0)
                return "";
            else if (n <= 19)
                return new string[] {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
         "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
         "Seventeen", "Eighteen", "Nineteen"}[n - 1] + " ";
            else if (n <= 99)
                return new string[] {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy",
         "Eighty", "Ninety"}[n / 10 - 2] + " " + NumberToText(n % 10);
            else if (n <= 199)
                return "One Hundred " + NumberToText(n % 100);
            else if (n <= 999)
                return NumberToText(n / 100) + "Hundreds " + NumberToText(n % 100);
            else return "One Thousand ";
           
        }
    }
}
