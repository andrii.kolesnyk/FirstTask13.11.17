﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    public static class BasicFunctionality
    {
        public static void ShowMenu()
        {
            Console.WriteLine("\tMain menu");
            Console.WriteLine("Enter the value from 1 to 8 : ");
            Console.WriteLine("1.Find the min element of array");
            Console.WriteLine("2.Find the max element of array");
            Console.WriteLine("3.Find the sum elements of array");
            Console.WriteLine("4.Sort array by descending");
            Console.WriteLine("5.Sort array by ascending");
            Console.WriteLine("6.Show array");
            Console.WriteLine("7.Additional task 1 (create new array)");
        }
        public static void MinElement(ArrayList array)
        {
            array.Sort();
            Console.WriteLine("Min element of array is " + array[0]);
            Console.WriteLine("Press any button to continue...");
            Console.ReadKey();
        }
        public static void MaxElement(ArrayList array)
        {
            array.Sort();
            Console.WriteLine("Max element of array is " + array[array.Count-1]);
            Console.WriteLine("Press any button to continue...");
            Console.ReadKey();
        }
        private static void DetermineTheTypeForSum(ArrayList array,ref double score)
        {
            score = 0;
            double[] arrayForCalculation = new double[array.Count];
            array.CopyTo(arrayForCalculation);
            for (int i = 0; i < array.Count; i++)
                score += (double)array[i];
        }
        private static void DetermineTheTypeForSum(ArrayList array, ref int score)
        {
            score = 0;
            int[] arrayForCalculation = new int[array.Count];
            array.CopyTo(arrayForCalculation);
            for (int i = 0; i < array.Count; i++)
                score += (int)array[i];
        }
        public static void SumElements(ArrayList array)
        {
            int scoreInt = 0;
            double scoreDouble = 0;
            try
            {
                DetermineTheTypeForSum(array,ref scoreInt);
                Console.WriteLine("Sum of elements of array is " + scoreInt);
            }
            catch
            {
                DetermineTheTypeForSum(array,ref scoreDouble);
                Console.WriteLine("Sum of elements of array is " + scoreDouble);
            }
            Console.WriteLine("Press any button to continue...");
            Console.ReadKey();
        }
        public static void SortByDescending(ArrayList array)
        {
            array.Sort();
            Console.WriteLine("Sorted Array:\n");
            for(int i = array.Count-1; i >= 0; i--)
                Console.WriteLine(array[i] + " ");
            Console.WriteLine("\nPress any button to continue...");
            Console.ReadKey();
        }
        public static void SortByAscending(ArrayList array)
        {
            array.Sort();
            Console.WriteLine("Sorted Array:\n");
            for (int i = 0; i < array.Count; i++)
                Console.WriteLine(array[i] + " ");
            Console.WriteLine("\nPress any button to continue...");
            Console.ReadKey();
        }
        public static void ShowArray(ArrayList array)
        {
            int varificator;
            Console.WriteLine("Your array:");
            for (int i = 0; i < array.Count; i++)
            {
                if (int.TryParse(array[0].ToString(), out varificator))
                    Console.WriteLine(array[i] + " - " + AdditionalFeatures.NumberToText((int)array[i]));
                else
                    Console.WriteLine(array[i]);
            }
            Console.WriteLine("\nPress any button to continue...");
            Console.ReadKey();
        }
    }
}
