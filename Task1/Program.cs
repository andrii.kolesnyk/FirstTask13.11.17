﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void MainWindow(int lenghtOfArray)
        {
            ArrayList array = new ArrayList();
            FillArray(array, lenghtOfArray);
            while(true)
            {
                Console.Clear();
                BasicFunctionality.ShowMenu();
                switch(InputHandler())
                {
                    case 1: BasicFunctionality.MinElement(array);
                        break;
                    case 2: BasicFunctionality.MaxElement(array);
                        break;
                    case 3: BasicFunctionality.SumElements(array);
                        break;
                    case 4: BasicFunctionality.SortByDescending(array);
                        break;
                    case 5: BasicFunctionality.SortByAscending(array);
                        break;
                    case 6: BasicFunctionality.ShowArray(array);
                        break;
                    case 7: AdditionalFeatures.CreateNewArray(array);   
                        break;
                    default:
                        {
                            Console.WriteLine("You chose wrong number. Press any button and try again, please.");
                            Console.ReadKey();
                            break;
                        }
                }
            }
        }
        static void FillArray(ArrayList array, int lenghtOfArray)
        {
            Random rnd = new Random();
            for (int i = 0; i < lenghtOfArray; i++)
                array.Add(rnd.Next(0, 1000));
        }
        static int InputHandler()
        {
            int lenght = 0;
            var isInt = int.TryParse(Console.ReadLine(), out lenght);
            if (!isInt || lenght <= 0)
            {
                Console.Clear();
                BasicFunctionality.ShowMenu();
                Console.WriteLine("Wrong number. Try again, please.");
                lenght = InputHandler();
            }
            return lenght;
        }
        static void Main(string[] args)
        {
            BasicFunctionality.ShowMenu();
            Console.WriteLine("\nEnter the lenght of array:");
            var lenghtOfArray = InputHandler();
            MainWindow(lenghtOfArray); 
        }
    }
}
