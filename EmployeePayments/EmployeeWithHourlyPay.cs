﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    class EmployeeWithHourlyPay : Employee
    {
        public int WorkingHours;
        public double PaymentPerHour;
        public override double AverageSalary
        {
            get
            {
                return WorkingHours * PaymentPerHour;
            }
            set
            {
                AverageSalary = value;
            }
        }

        public EmployeeWithHourlyPay(int id,string firstName,string lastName, int workingHours, double paymentPerHour)
            :base(id,firstName,lastName)
        {
            EmployeeDataChecker.TryToAssignWorkingHours(workingHours, out WorkingHours);
            EmployeeDataChecker.TryToAssignSalary(paymentPerHour,out PaymentPerHour);
        }
    }
}
