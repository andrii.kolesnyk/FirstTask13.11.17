﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    static class BasicFeatures
    {

        static List<Employee> listOfEmployees = new List<Employee>();
   
        static BasicFeatures()
        {
            FillListForTestingProg();
            listOfEmployees = listOfEmployees.OrderBy(x => x, new EmployeeComparer()).ToList();
        }
        static private void FillListForTestingProg()
        {
            listOfEmployees.Add(new EmployeeWithFixedSalary(1, "Andrii", "Simonov", 10000));
            listOfEmployees.Add(new EmployeeWithHourlyPay(7, "Kevin", "Abranchuk", 400, 40));
            listOfEmployees.Add(new EmployeeWithHourlyPay(6, "Bogdan", "Baranchuk", 400, 40));
            listOfEmployees.Add(new EmployeeWithFixedSalary(2, "Sergii", "LastName", 6000));
            listOfEmployees.Add(new EmployeeWithHourlyPay(3, "Max", "Kolboy", 300, 50));
            listOfEmployees.Add(new EmployeeWithFixedSalary(4, "Vadim", "Lenin", 14959));
            listOfEmployees.Add(new EmployeeWithFixedSalary(5, "Peter", "Griffin", 11000));
            listOfEmployees.Add(new EmployeeWithHourlyPay(8, "Stuwie", "Swallow", 400, 40));
            listOfEmployees.Add(new EmployeeWithHourlyPay(9, "John", "Levanovski", 200, 50));
        }
        static private void PrintEmployees(List<Employee> listToPrint)
        {
            foreach (var item in listToPrint)
                Console.WriteLine($"ID: {item.Id} | Last name: {item.LastName} | First name: {item.FirstName} | Average salary = {item.AverageSalary}");
        }
        static public void TakeEmployees(int numberOfEmployees)
        {
            try
            {
                if (numberOfEmployees > listOfEmployees.Count || numberOfEmployees <= 0)
                    throw new Exception();

                Console.WriteLine("Taken employees :");
                var takenEmployees = listOfEmployees.Take(numberOfEmployees).ToList();
                PrintEmployees(takenEmployees);
            }
            catch
            {
                Console.WriteLine("Incorrect value. Try again, please...");
            }
        }
        static public void SortEmployees()
        { 
            listOfEmployees = listOfEmployees.OrderBy(x => x, new EmployeeComparer()).ToList();
            PrintEmployees(listOfEmployees);
        }
    }
}
