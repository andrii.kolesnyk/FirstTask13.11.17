﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    static class ConsoleUI
    {
        static public void Start()
        {
            while (true)
            {
                Console.Clear();
                MainMenu();
                switch (SwitchInputValidator.CheckTheNumber(Console.ReadLine()))
                {
                    case 1:
                        {
                            Console.WriteLine("Enter how much employees you want to take from list : ");
                            BasicFeatures.TakeEmployees(SwitchInputValidator.CheckTheNumber(Console.ReadLine()));
                            Console.ReadKey();
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Incorrect value. Press any button and try again, please...");
                            Console.ReadKey();
                            break;
                        }
                }
            }
        }
        static void MainMenu()
        {
            Console.WriteLine("\t Main menu \n");
            Console.WriteLine("1.Take N employees");
            Console.WriteLine("\nSorted list of employees is shown below:\n");
            BasicFeatures.SortEmployees();
        }
    }
}
