﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    class EmployeeWithFixedSalary : Employee
    {
        public double FixedMonthSalary;
        public override double AverageSalary
        {
            get
            {
                return FixedMonthSalary;
            }
            set
            {
                AverageSalary = value;
            }
        }
        public EmployeeWithFixedSalary(int id, string firstName, string lastName, double monthSalary)
            : base(id, firstName, lastName) => EmployeeDataChecker.TryToAssignSalary(monthSalary, out FixedMonthSalary);
    }
}
