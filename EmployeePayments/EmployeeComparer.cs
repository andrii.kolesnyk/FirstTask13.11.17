﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    class EmployeeComparer : IComparer<Employee>
    {
        public int Compare(Employee employee1, Employee employee2)
        {
            if (employee1.AverageSalary > employee2.AverageSalary)
                return -1;
            else if (employee1.AverageSalary < employee2.AverageSalary)
                return 1;
            else return employee1.LastName.CompareTo(employee2.LastName);
        }
    }
}
