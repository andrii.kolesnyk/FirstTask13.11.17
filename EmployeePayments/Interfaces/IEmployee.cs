﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    interface IEmployee
    {
        int Id { set; get; }
        string FirstName { set; get; }
        string LastName { set; get; }
        double AverageSalary { get; set; }
    }
}
