﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    static public class SwitchInputValidator
    {
        static public int CheckTheNumber(string input)
        {
            int menuSelector;
            int.TryParse(input, out menuSelector);
            return menuSelector;
        }
    }
}
