﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    static class EmployeeDataChecker
    {
        static public void TryToAssignSalary(double salary, out double payment)
        {
            if (salary >= 0)
                payment = salary;
            else throw new Exception("Incorrect salary for employee.");
        }
        static public void TryToAssignWorkingHours(int workingHoursToAssign, out int workingHoursOfEmployee)
        {
            if (workingHoursToAssign > 0)
                workingHoursOfEmployee = workingHoursToAssign;
            else throw new Exception("Incorrect working hours for employee.");
        }
    }
}
