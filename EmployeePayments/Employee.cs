﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayments
{
    abstract class Employee : IEmployee 
    {
        public int Id { set; get; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        abstract public double AverageSalary { get; set; }
        public Employee() {}
        public Employee(int id,string firstName,string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }


    }
}
