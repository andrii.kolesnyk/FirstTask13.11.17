
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/21/2017 18:11:13
-- Generated from EDMX file: C:\Users\koles\source\repos\Task1\DataBase\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [DataBaseTest];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'FootballerSet'
CREATE TABLE [dbo].[FootballerSet] (
    [Id] tinyint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Position] tinyint  NOT NULL,
    [ClubId] int  NOT NULL
);
GO

-- Creating table 'ClubSet'
CREATE TABLE [dbo].[ClubSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Trainer] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'FootballerSet'
ALTER TABLE [dbo].[FootballerSet]
ADD CONSTRAINT [PK_FootballerSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClubSet'
ALTER TABLE [dbo].[ClubSet]
ADD CONSTRAINT [PK_ClubSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ClubId] in table 'FootballerSet'
ALTER TABLE [dbo].[FootballerSet]
ADD CONSTRAINT [FK_ClubFootballer]
    FOREIGN KEY ([ClubId])
    REFERENCES [dbo].[ClubSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClubFootballer'
CREATE INDEX [IX_FK_ClubFootballer]
ON [dbo].[FootballerSet]
    ([ClubId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------