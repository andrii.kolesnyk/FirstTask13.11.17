﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase
{
    interface IFootballer : IGenericRepository
    {
        void ChangeClub(int clubId);
        void ChangeTrainer(int trainerId);
        void GetClub();
    }
}
