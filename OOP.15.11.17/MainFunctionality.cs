﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP._15._11._17
{
    static class MainFunctionality
    { 
        static private List<Figure> listOfFigures = new List<Figure>();
        static int[] Input()
        {
            Console.Clear();
            Console.WriteLine("Input order");
            Console.WriteLine("1***********2");
            Console.WriteLine("*           *");
            Console.WriteLine("*           *");
            Console.WriteLine("3***********4");
            Console.WriteLine("Enter coordinates like here : 1 2 3 4 5 6 7 8");
            string input = Console.ReadLine();
            string[] arrayOfStrings = input.Split(' ');
            int[] inputResult = arrayOfStrings.Select(x => int.Parse(x)).ToArray<int>();
            return inputResult;
        }
        static void ParseArray(int[] array)
        {
            if (Verifycator.IsSquare(array))
            {
                Square squa = new Square(array);
                listOfFigures.Add(squa);
            }
            else if (Verifycator.IsRectangle(array))
            {
                Rectangle rect = new Rectangle(array);
                listOfFigures.Add(rect);
            }
            else
            {
                Console.WriteLine("Wrong figure.");
                Console.ReadKey();
            }
        }
        static public void Start()
        {
            while (true)
            {
                Console.Clear();
                ShowMenu();
                switch (int.Parse(Console.ReadLine()))
                {
                    case 1: ParseArray(Input());
                        break;
                    case 2: ShowFigures(true);
                        break;
                    case 3: ShowFigures(false);
                        break;
                    default:
                        {
                            Console.WriteLine("You chose wrong number. Press any button and try again, please.");
                            Console.ReadKey();
                            break;
                        }
                }
            }
        }
        private static void ShowFigures(bool rectangle)
        {
            foreach(var item in listOfFigures)
            {
                if (item is Rectangle && rectangle)
                {
                    item.Draw();
                }
                else if (item is Square && !rectangle)
                {
                    item.Draw();
                }
            }
        }
        static void ShowMenu()
        {
            Console.WriteLine("1.Enter the coordinates of figure");
            Console.WriteLine("2.Show all rectangles");
            Console.WriteLine("3.Show all squares");
        }
    }
}
