﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP._15._11._17
{
    static class Verifycator
    {
        static public bool IsRectangle(int[] coordinates)
        {
            if (coordinates.Length != 8)
                return false;
            if (coordinates[0] == coordinates[4] && coordinates[1] == coordinates[3] && coordinates[2] == coordinates[6] && coordinates[5] == coordinates[7])
                return true;
            else return false;
        }
        static public bool IsSquare(int[] coordinates)
        {
            if (IsRectangle(coordinates))
            {
                double[,] vectors = new double[2,2];
                vectors[0, 0] = coordinates[2] - coordinates[0];
                vectors[0, 1] = coordinates[3] - coordinates[1];
                vectors[1, 0] = coordinates[4] - coordinates[0];
                vectors[1, 1] = coordinates[5] - coordinates[1];
                double oneSide = Math.Sqrt(vectors[0, 0] * vectors[0, 0] + vectors[0, 1] * vectors[0, 1]);
                double secondSide = Math.Sqrt(vectors[1, 0] * vectors[1, 0] + vectors[1, 1] * vectors[1, 1]);
                return oneSide == secondSide;
            }
            else return false;
        }
    }
}
