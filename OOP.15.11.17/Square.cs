﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP._15._11._17
{
    class Square : Figure
    {
        public Square() { }
        public Square(int[] array)
        {
            Input(array);
        }
        public override void Input(int[] coordinates)
        {
            for (int i = 0; i < coordinates.Length - 1; i += 2)
            {
                Point pnt = new Point(coordinates[i], coordinates[i + 1]);
                ListOfPoints.Add(pnt);
            }
        }
        public override void Draw()
        {
            Console.WriteLine("Coordinates of square: ");
            foreach (var item in ListOfPoints)
            {
                Console.Write("(X = " + item.X + " ; Y = " + item.Y + ")\n");
            }
            Console.ReadKey();
        }
    }
}
