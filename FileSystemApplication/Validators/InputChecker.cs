﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemApplication
{
    static class InputChecker
    {
        static public void AddLevelChecker(int numberOfLevelsToAdd, int currentLevel, int maxLevel)
        {
            if (numberOfLevelsToAdd <= 0)
                throw new Exception("Incorrect value. Try again...");
            else if (numberOfLevelsToAdd > (maxLevel - currentLevel))
                throw new Exception("It will be more than max level");
        }
        static public void RemoveLevelChecker(int numberOfLevelsToAdd, int numberOfCurrentLevels)
        {
            if (numberOfLevelsToAdd <= 0)
                throw new Exception("Incorrect value. Try again...");
            else  if (numberOfLevelsToAdd > numberOfCurrentLevels)
                throw new Exception("Something went wrong. Check the number of existing levels.");
        }
    }
}
