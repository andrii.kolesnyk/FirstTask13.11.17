﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeePayments;

namespace FileSystemApplication
{
    static class ConsoleUI
    {
        static public void Start()
        {
            while (true)
            {
                Console.Clear();
                MainMenu();
                switch (SwitchInputValidator.CheckTheNumber(Console.ReadLine()))
                {
                    case 1:
                        {
                            Console.WriteLine("Enter how much levels you want to add : ");
                            LevelCreator.AddLevel(SwitchInputValidator.CheckTheNumber(Console.ReadLine()));
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter how much levels you want to remove : ");
                            LevelCreator.RemoveLevel(SwitchInputValidator.CheckTheNumber(Console.ReadLine()));
                            break;
                        }
                }
            }
        }
        static void MainMenu()
        {
            Console.WriteLine("\t Main menu ");
            Console.WriteLine("Max level = 10\n");
            Console.WriteLine("1.Add level");
            Console.WriteLine("2.Remove level");
        }
    }
}
