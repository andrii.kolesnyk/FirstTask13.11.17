﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemApplication
{
    static class LevelCreator 
    {
        static LevelController LvlLogic;

        static LevelCreator()
        {
            LvlLogic = new LevelController();
        }
        static public void AddLevel(int numberOfLevels)
        {
            LvlLogic.AddFeature(numberOfLevels);
        }
        static public void RemoveLevel(int numberOfLevels)
        {
            LvlLogic.RemoveFeature(numberOfLevels);
        }
    }
}
