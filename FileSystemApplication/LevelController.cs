﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemApplication
{
    /// <summary>
    /// PathToFolder - path to current folder
    /// DrInf - for working with current directory
    /// CurrentLevel - pointer on the current folder
    /// </summary>
     class LevelController
     {
        Stack<DirectoryInfo> StackOfFolders = new Stack<DirectoryInfo>();
        Stack<FileStream> StackOfFiles = new Stack<FileStream>();
        string PathToFolder = Environment.CurrentDirectory + @"\FolderProgram";
        DirectoryInfo DrInf;
        readonly int MaxLevel = 10;
        int CurrentLevel = -1;

        public LevelController()
        {
            DrInf = new DirectoryInfo(PathToFolder);
            if (!Directory.Exists(PathToFolder))
                Directory.CreateDirectory(PathToFolder);
        }

        /// <summary>
        /// If root folder is not empty, collect all files
        /// </summary>
        private void StartCollectFiles()
        {
            try
            {
                if (CurrentLevel == -1)
                    GetFolders();
            }
            catch
            {
                CurrentLevel = -1;
                StackOfFiles.Clear();
                StackOfFolders.Clear();
                throw new Exception();
            }
        }

        private void GetFolders()
        {
            var pathToFolder = Directory.GetDirectories(PathToFolder);
            var filePath =  Directory.GetFiles(PathToFolder);
            ProperlyOrderChecker.IsCorrectWay(pathToFolder, filePath);
            if (pathToFolder.Any())
            {
                StackOfFolders.Push(new DirectoryInfo(pathToFolder[0]));
                using (FileStream fileStream = new FileStream(filePath[0], FileMode.OpenOrCreate))
                    StackOfFiles.Push(fileStream);
                PathToFolder = pathToFolder[0];
                CurrentLevel = int.Parse(pathToFolder[0].ToCharArray().Last().ToString());
                DrInf = new DirectoryInfo(PathToFolder);
                GetFolders();
            }
            else return;
        }

        public void AddFeature(int levelToAdd)
        {
            try
            {
                InputChecker.AddLevelChecker(levelToAdd, StackOfFolders.Count, MaxLevel);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return;
            }
            try
            {
                StartCollectFiles();
                AddLogic(levelToAdd);
            }
            catch
            {
                Console.WriteLine("Something wrong with your files. Check it and fix manually. Press any button to continue...");
                CurrentLevel = -1;
                Console.ReadKey();
            }
        }

        private void AddLogic(int levelToAdd)
        {
            CurrentLevel = CurrentLevel + 1; //move pointer to the next folder 
            for (int i = CurrentLevel; i < CurrentLevel + levelToAdd; i++)
            {
                DrInf.CreateSubdirectory($"level_{i}");
                using (FileStream fileStream = new FileStream(PathToFolder + $"\\file_level_{i}.txt", FileMode.OpenOrCreate))
                    StackOfFiles.Push(fileStream);
                PathToFolder += $"\\level_{i}";
                DrInf = new DirectoryInfo(PathToFolder);
                StackOfFolders.Push(DrInf);
            }
            CurrentLevel += levelToAdd - 1; // move back pointer on the current folder
        }

        private void RemoveLogic(int numberOfLevels)
        {
            for (int i = 0; i < numberOfLevels; i++)
            {
                File.Delete(StackOfFiles.Pop().Name);
                StackOfFolders.Pop().Delete(true);
                CurrentLevel -= 1;
                PathToFolder = PathToFolder.Substring(0, PathToFolder.Length - 8);
            }
            DrInf = new DirectoryInfo(PathToFolder); // do it for add feature
        }

        public void RemoveFeature(int numberOfLevels)
        {
            try
            {
                InputChecker.RemoveLevelChecker(numberOfLevels, StackOfFolders.Count);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return;
            }
            try
            {
                StartCollectFiles();
                RemoveLogic(numberOfLevels);
            }
            catch
            {
                Console.WriteLine("Something wrong with your files. Check it and fix manually. Press any button to continue...");
                CurrentLevel = -1;
                Console.ReadKey();
            }
        }
    }
}
