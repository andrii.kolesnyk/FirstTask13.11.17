﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateLamdaEvents
{
    class Program
    {
        delegate double Operation(double value1, double value2);
        static double[] Results = new double[4]; 
        static double Multiply(double value1,double value2)
        {
            return value1 + value2;
        }
        static double Sum(double value1, double value2)
        {
            return value1 + value2;
        }
        static double Subtraction(double value1, double value2)
        {
            return value1 - value2;
        }
        static double Division(double value1, double value2)
        {
            return value1 / value2;
        }
        static void Main(string[] args)
        {
            Operation substraction = delegate (double value1, double value2)
            {
                return value1 - value2;
            };
            Operation multiply = delegate (double value1, double value2)
            {
                return value1 * value2;
            };
            Func<double, double, double> division = Division; 
            Func<double,double,double> sum =  (value1,  value2) =>
            {
                return value1 + value2;
            };
            Console.WriteLine(substraction(1, 2));
            Console.WriteLine(multiply(1, 2));
            Console.WriteLine(division(1, 2));
            Console.WriteLine(sum(1, 2));
            Console.ReadKey();
            
        }
    }
}
